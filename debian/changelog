opensmtpd-extras (6.7.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.

  [ Ryan Kavanagh ]
  * Allow cross-building, 08_cross.patch.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #958485)
  * Let gbp-dch parse meta information

 -- Ryan Kavanagh <rak@debian.org>  Mon, 25 May 2020 12:56:51 -0400

opensmtpd-extras (6.7.1-1) unstable; urgency=medium

  * New upstream release
    + We no longer need libasr, drop build-dependency
  * Set compat level to 13
  * Bump standards version to 4.5.0
  * Set Rules-Requires-Root: no

 -- Ryan Kavanagh <rak@debian.org>  Wed, 20 May 2020 18:21:00 -0400

opensmtpd-extras (6.6.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove trailing whitespaces

  [ Ryan Kavanagh ]
  * New upstream release (Closes: #944453)
    + Dropped the following patches. They are no longer needed or were applied
      upstream: 01_install_man.diff, 02_table_sqlite.5.diff,
      03_PATH_CHROOT.diff, 04_manpag_typo.diff, 05_TABLE_POSTGRES.diff,
      06_filter_api_typo.diff.
  * Bump standards-version to 4.4.1
  * Updated copyright file
  * Drop unneeded NEWS file
  * Fix typos in manpages, 01_manpage_typos.diff
  * This version of opensmtpd-extras needs at least opensmtpd 6.6.1p1. Update
    depends accordingly.
  * Added missing build-dependency on pkg-config

 -- Ryan Kavanagh <rak@debian.org>  Tue, 19 Nov 2019 15:34:04 -0500

opensmtpd-extras (6.4.0-1) experimental; urgency=medium

  * New upstream release
    + Uploading to experimental because of incompatibility with
      opensmtpd version in unstable
  * Bump compat to 12
  * Bump standards version to 4.4.0
  * Update Vcs-* fields to point to experimental branch

 -- Ryan Kavanagh <rak@debian.org>  Sun, 18 Aug 2019 14:48:37 -0400

opensmtpd-extras (5.7.1-4) unstable; urgency=medium

  * Don't include leading slash in install files
  * Set branch to debian/sid in Vcs-Git
  * Bump standards version to 4.1.3
  * Update Vcs-* to point to salsa
  * Drop opensmtpd-extras-experimental: filters are currently broken due to an
    API mismatch with the current version of opensmtpd (Closes: #873731)
  * Dropping filters also means we don't need libssl1.0-dev (Closes: #859545)
  * Change priority from extra to optional
  * Update watch file to connect over https
  * Override depends-on-mail-transport-agent-without-alternatives: we
    legitimately need opensmtpd
  * Turn on hardening

 -- Ryan Kavanagh <rak@debian.org>  Sun, 25 Mar 2018 18:39:37 -0400

opensmtpd-extras (5.7.1-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Switch to debhelper compat 10, autoreconf fixes building with
    other automake versions. (Closes: #866994)
  * Add "foreign" option to AM_INIT_AUTOMAKE, to fix autoreconf.

 -- Adrian Bunk <bunk@debian.org>  Mon, 31 Jul 2017 10:55:05 +0300

opensmtpd-extras (5.7.1-3) unstable; urgency=medium

  [ Ryan Kavanagh ]
  * Fix typo in bug number in changelog
  * Split experimental filters into an -experimental package. Warn users of
    the split via a NEWS file.
  * Force OpenSSL version to be 1.0.x (Closes: #828474)
  * Transition from libmysqlclient-dev to default-libmysqlclient-dev
  * Bump standards version to 3.9.8
  * Fix typo in filter_api.c, 06_filter_api_typo.diff

  [ Jonas Maurus ]
  * Fix segfault in table_postgres if connections suddenly close and
    PQresultErrorField returns NULL. 05_TABLE_POSTGRES.diff

 -- Ryan Kavanagh <rak@debian.org>  Thu, 24 Nov 2016 20:01:58 -0500

opensmtpd-extras (5.7.1-2) unstable; urgency=medium

  * Don't ignore --with-privsep-path configure option, 03_PATH_CHROOT.diff
    (Closes: #812537)
  * Bump Standards-Version to 3.9.7
  * Fix typo in filter-pause(8), 04_manpage_typo.diff
  * Transition Vcs-Git URL from git:// to https://

 -- Ryan Kavanagh <rak@debian.org>  Sat, 20 Feb 2016 17:22:29 -0500

opensmtpd-extras (5.7.1-1) unstable; urgency=low

  * Initial release (Closes: #803688)

 -- Ryan Kavanagh <rak@debian.org>  Sun, 01 Nov 2015 21:03:14 -0500
